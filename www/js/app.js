// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic','ionic.service.core',  'ionic.service.push', 'angular-md5', 'starter.controllers', 'starter.services', 'ui.router', 'UserValidation', 'ngCookies', 'ngStorage', 'firebase', 'ngCordova'])

.run(function($ionicPlatform, $ionicPopup, $state, $ionicHistory, $rootScope, $ionicNavBarDelegate) {
  $ionicPlatform.ready(function() {
    // Check for network connection
    if(window.Connection) {
      if(navigator.connection.type == Connection.NONE || navigator.connection.type == Connection.UNKNOWN) {
        $ionicPopup.confirm({
          title: "Sin conexión",
          content: "No se ha detectado una conexión a internet. Por favor conectese e inténtelo de nuevo."
        })
        .then(function(result) {
          if(!result) {
            ionic.Platform.exitApp();
          }
        });
      }
    }
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
    $ionicPlatform.registerBackButtonAction(function (event) {
      if($state.current.name=="app.dashboard" || $state.current.name=="login"){
        $ionicPopup.confirm({
          title: 'Alerta del sistema',
          template: 'Cerrar aplicación?'
        }).then(function(res) {
          if (res) {
            ionic.Platform.exitApp();
          }
        })
      }
      else {
        navigator.app.backHistory();
      }

    }, 100);

    $rootScope.$on('$stateChangeSuccess', function() {
      if(toState.name.indexOf('main') !== -1) {
        $ionicNavBarDelegate.showBackButton(true);
      }
    });


    //PUSH!
    var push = new Ionic.Push({
      "debug": false
    });
    push.register(function(token) {
      console.log("My Device token:",token.token);
      localStorage.setItem("device_token", token.token);
      push.saveToken(token);  // persist the token in the Ionic Platform
    });

  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('login', {
    url: '/login',
    cache: false,
    templateUrl: 'templates/login.html',
    controller: 'LoginCtrl'
  })

  .state('reset_password', {
    url: '/reset_password',
    templateUrl: 'templates/reset_password.html',
    controller: 'LoginCtrl'
  })

  .state('reset_password_response', {
    url: '/reset_password_response',
    templateUrl: 'templates/reset_password_response.html',
    controller: 'LoginCtrl'
  })

  .state('register_index', {
    url: '/register_index',
    templateUrl: 'templates/registration_index.html',
    controller: 'RegistrationCtrl'
  })

  .state('register', {
    url: '/register',
    templateUrl: 'templates/registration.html',
    controller: 'RegistrationCtrl'
  })

  .state('register_response', {
    url: '/register_response',
    templateUrl: 'templates/registration_response.html',
    controller: 'RegistrationCtrl'
  })

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.dashboard', {
    url: '/dashboard',
    params: {
      apply: 0,
      welcome: 0,
    },
    cache: false,

    views: {
      'menuContent' :{
        templateUrl: "templates/dashboard.html",
        controller: 'DashboardCtrl'
      }
    }
  })
  
  .state('app.settings', {
    url: '/settings',
    views: {
      'menuContent' :{
        templateUrl: 'templates/settings.html',
        controller: 'SettingsCtrl'
      }
    }
  })

  .state('app.notifications', {
    url: '/notifications',
    views: {
      'menuContent' :{
        templateUrl: 'templates/notifications.html',
        controller: 'NotificationsCtrl'
      }
    }
  })

  .state('app.notification', {
    url: '/notifications/:notificationId',
    views: {
      'menuContent' :{
        templateUrl: 'templates/notification.html',
        controller: 'NotificationCtrl'
      }
    }
  })


  .state('app.messages', {
    url: '/messages',
    views: {
      'menuContent' :{
        templateUrl: 'templates/messages.html',
        controller: 'MessagesCtrl'
      }
    }
  })

  .state('app.message', {
    url: '/messages/:messageId',
    views: {
      'menuContent' :{
        templateUrl: 'templates/message.html',
        controller: 'MessageCtrl'
      }
    }
  })


  .state('app.resume', {
    url: '/resume',
    cache: false,
    views: {
      'menuContent' :{
        templateUrl: "templates/my_cv.html",
        controller: 'DashboardCtrl'
      }
    }
  })

  .state('app.jobs', {
    url: '/jobs',
    cache: false,
    views: {
      'menuContent' :{
        templateUrl: "templates/jobs.html",
        controller: 'JobsCtrl'
      }
    }
  })

  .state('app.my_jobs', {
    url: '/my_jobs',
    cache: false,
    views: {
      'menuContent' :{
        templateUrl: "templates/my_jobs.html",
        controller: 'MyJobsCtrl'
      }
    }
  })

  .state('app.search', {
    url: '/search',
    cache: false,
    views: {
      'menuContent' :{
        templateUrl: "templates/search.html",
        controller: 'SearchCtrl'
      }
    }
  })

  .state('app.search_country', {
    url: '/search_country',
    cache: false,
    views: {
      'menuContent' :{
        templateUrl: "templates/search_country.html",
        controller: 'SearchCountryCtrl'
      }
    }
  })

  .state('app.search_position_area', {
    url: '/search_position_area',
    cache: false,
    views: {
      'menuContent' :{
        templateUrl: "templates/search_position_area.html",
        controller: 'SearchPACtrl'
      }
    }
  })

  .state('app.search_position_group', {
    url: '/search_position_group',
    cache: false,
    views: {
      'menuContent' :{
        templateUrl: "templates/search_position_group.html",
        controller: 'SearchPGCtrl'
      }
    }
  })

  .state('app.search_contract', {
    url: '/search_contract',
    cache: false,
    views: {
      'menuContent' :{
        templateUrl: "templates/search_contract.html",
        controller: 'SearchContractCtrl'
      }
    }
  })

  .state('app.job', {
    url: "/jobs/:jobId",
    cache: false,
    views: {
      'menuContent' :{
        templateUrl: "templates/job.html",
        controller: 'JobCtrl'
      }
    }
  })

  .state('app.job_process', {
    url: "/jobs/:jobId/process",
    cache: false,
    views: {
      'menuContent' :{
        templateUrl: "templates/applied_job.html",
        controller: 'AppliedJobCtrl'
      }
    }
  })

  .state('app.company', {
    url: "/jobs/:jobId/company/:companyID",
    views: {
      'menuContent' :{
        templateUrl: "templates/company.html",
        controller: 'CompanyCtrl'
      }
    }
  })
  
  .state('app.detail', {
    url: "/jobs/:jobId/detail",
    views: {
      'menuContent' :{
        templateUrl: "templates/job_detail.html",
        controller: 'JobCtrl'
      }
    }
  });
  
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');
});


