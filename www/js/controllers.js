angular.module('starter.controllers', [])

.controller('LoginCtrl', function($scope, $localStorage, $timeout, $window, $http, $log, $state, md5, $httpParamSerializerJQLike, $cookies, $ionicLoading, $ionicNavBarDelegate, $location, $ionicHistory, $cordovaInAppBrowser) {
  
  //localStorage.setItem("jobId", id);
  $scope.loginData = {};
  $scope.forgotPass = {};


  
  $scope.data = '';
  $scope.headers = '';
  $scope.config = '';
  $scope.LoginError = false;

  if(localStorage.getItem("idcandidate")!=undefined || localStorage.getItem("idcandidate")!=null){
    $http({
      url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/getCountNewNotifyByUser',
      method: "POST",
      data: $httpParamSerializerJQLike({
        "idcandidate": localStorage.getItem("idcandidate")
      }),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
    })
    .success(function(data, status, headers, config) {
      $scope.showResponse = true;
      localStorage.setItem("notificationsNumber", angular.fromJson(data)); 
      $scope.config = config;
    })
    .error(function(data, status,headers,config) {
      $scope.showResponse = false;
      $scope.config = config;
    }); 

    $state.go("app.dashboard", {welcome: 1, appliedJob: 0}, {reload: true});
  }

  // Función para el login del usuario
  $scope.doLogin = function() {
    $ionicLoading.show({
      content: 'Loading',
      animation: 'fade-in',
      showBackdrop: true,
      maxWidth: 400,
      showDelay: 0
    });

    $http({
      //url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/getUserById',
      url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/getUserinfo',
      method: "POST",
      data: $httpParamSerializerJQLike({
        email: $scope.loginData.email,
        pwd: md5.createHash($scope.loginData.password || '')
      }),
  
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
    })
    .success(function(data, status, headers, config) {
      $scope.data = angular.fromJson(data); 
      $scope.config = config;
      localStorage.setItem("idcandidate", angular.fromJson(data).id_candidate);
      localStorage.setItem("name", angular.fromJson(data).name);
      localStorage.setItem("lastname", angular.fromJson(data).lastname);
      localStorage.setItem("picture", angular.fromJson(data).pic);
      localStorage.setItem("searching", angular.fromJson(data).searching);
      localStorage.setItem("wherenotify", angular.fromJson(data).wherenotify);
      localStorage.setItem("notifyviews", angular.fromJson(data).notifyviews);
    })
    .error(function(data, status,headers,config) {
      $scope.LoginError = true;
      $scope.data = angular.fromJson(data);
      $scope.status = status;
      $scope.headers = headers;
      $scope.config = config;
    })
    .finally(function () {
      $ionicLoading.hide();
      if($scope.data.id_candidate==undefined || $scope.data.id_candidate==null){
        $scope.LoginError = true;
      }
      else{
        $state.go("app.dashboard", {welcome: 1}, {reload: true});
      }
    });

  };

  $scope.gotoJobs = function(){

    $timeout(function() {
      localStorage.clear();
      $ionicHistory.clearCache();
      $ionicHistory.clearHistory();
      $state.go('app.dashboard', {welcome: 1}, {reload: true} );
    }, 1000);
    
  };

  $scope.sendInstructions = function(){
    console.log('Reset Pass', $scope.forgotPass);
    $timeout(function() {
      $state.go('reset_password_response');
    }, 1000);
  };

  $scope.goToForgot = function(){

    document.addEventListener("deviceready", function () {
      $cordovaInAppBrowser.open('http://www.konectaempleo.com/forgotapp', '_blank')
        .then(function(event) {
          // success
        })
        .catch(function(event) {
          // error
        });

    }, false);
  };

  $scope.goToRegister = function(){

    document.addEventListener("deviceready", function () {
      $cordovaInAppBrowser.open('http://www.konectaempleo.com/registrokeapp', '_blank')
        .then(function(event) {
          // success
        })
        .catch(function(event) {
          // error
        });


    }, false);
  };

})

.controller('RegistrationCtrl', function($scope, $localStorage, $cookies, $timeout, $http, $log,  md5, $httpParamSerializerJQLike, $stateParams, $state, $ionicLoading, $ionicNavBarDelegate, $location, $ionicHistory, $cordovaInAppBrowser) {

  $scope.registrationData = {};
  $scope.registerUser = function(form) {
    if($scope.registrationData.name != undefined && $scope.registrationData.lastname != undefined && $scope.registrationData.email != undefined 
      && $scope.registrationData.password == $scope.registrationData.password_c){

      console.log('Doing registration', $scope.registrationData);
      $timeout(function() {
        $state.go('register_response');
      }, 1000);
    }
  };

  $scope.destroySession = function(){
    $ionicHistory.clearCache().then(function() {
      //now you can clear history or goto another state if you need
      localStorage.clear();
      $ionicHistory.clearHistory();
      $ionicHistory.nextViewOptions({ disableBack: true, historyRoot: true });
      //window.location = "login.html"; 
      window.location = "index.html"
      //$state.go('login', {}, {reload: true});
    })
  };

  $scope.goToRegister = function(){

    document.addEventListener("deviceready", function () {
      $cordovaInAppBrowser.open('http://www.konectaempleo.com/registrokeapp', '_blank')
        .then(function(event) {
          // success
        })
        .catch(function(event) {
          // error
        });


    }, false);
  };

  $scope.registrationResponse = { message: "Tu registro ha sido exitoso" }; 
})

.controller('AppCtrl', function($scope, $localStorage, $cookies, $stateParams, $state, $httpParamSerializerJQLike, $timeout, $ionicLoading, $ionicNavBarDelegate, $location, $ionicHistory) {
  // Request de las preferencias del usuario ...
  //$scope.notificationsNumber = localStorage.getItem("notificationsNumber") == undefined ? 0 : localStorage.getItem("notificationsNumber");
  $scope.headers = '';
  $scope.config = '';

  var path = $location.path();
  //alert(localStorage.getItem("idcandidate"));
  if((path.indexOf('submit') != -1) || path == "/app/jobs" || path == "/app/dashboard"){
    $ionicNavBarDelegate.showBackButton(false);
    $ionicHistory.nextViewOptions({
      disableBack: true
    });
  }
  else{
    $ionicNavBarDelegate.showBackButton(true);    
  }

  if(path != "/app/dashboard"){
    //localStorage.setItem("appliedJob", 0);
  }

  if(localStorage.getItem("idcandidate")!=undefined || localStorage.getItem("idcandidate")!=null){
    $scope.showRegister = false;
    $scope.showMyJobs = true;
    $scope.showMyNotifications = true;
    $scope.showMyMessages = true;
    $scope.showPreferences = true;
    $scope.showCloseSession = true;
    $scope.showLogin = false;
    $scope.showDashboard = true;
    $scope.showMyCV = true;
  }
  else{
    $scope.showRegister = true;
    $scope.showMyJobs = false;
    $scope.showMyNotifications = false;
    $scope.showMyMessages = false;
    $scope.showPreferences = false;
    $scope.showCloseSession = false;
    $scope.showLogin = true;
    $scope.showMyCV = false;
  }

  $scope.destroySession = function(){
    $ionicHistory.clearCache().then(function() {
      //now you can clear history or goto another state if you need
      localStorage.clear();
      $ionicHistory.clearHistory();
      $ionicHistory.nextViewOptions({ disableBack: true, historyRoot: true });
      //window.location = "login.html"; 
      window.location = "index.html"
      //$state.go('login', {}, {reload: true});
    })
  };

  $scope.openJobs = function(){
    $location.url('/app/jobs');
    //window.location.reload(true);
  };

  $scope.goToSettings = function(){
    $state.go('app.settings')
  };

  $scope.goToCV = function(){
    $state.go('app.resume')
  };

  $scope.goToJobs = function(){
    $state.go('app.jobs')
  };

  $scope.goToMessages = function(){
    $state.go('app.messages')
  };

})

.controller('SettingsCtrl', function($scope, $localStorage, $cookies, $timeout, $http, $log,  $httpParamSerializerJQLike, $stateParams, $state, $ionicLoading, $ionicNavBarDelegate, $location, $ionicHistory, md5, $cordovaInAppBrowser) {
  
    $http({
      url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/getCountNewNotifyByUser',
      method: "POST",
      data: $httpParamSerializerJQLike({
        "idcandidate": localStorage.getItem("idcandidate")
      }),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
    })
    .success(function(data, status, headers, config) {
      $scope.showResponse = true;
      $scope.notificationsNumber = angular.fromJson(data); 
      $scope.config = config;
    })
    .error(function(data, status,headers,config) {
      $scope.notificationsNumber = 0;
      $scope.showResponse = false;
      $scope.config = config;
    }); 


  $scope.searchJobs = [
    { id: 0, title: "No"},
    { id: 1, title: "Si"}
  ];
  $scope.notificationMeans = [
    { id: 1, title: "App"},
    { id: 2, title: "Email"},
    { id: 3, title: "Email y app"},
    { id: 4, title: "No recibir alertas"}
  ];
  $scope.notificationViews = [
    { id: 0, title: "No"},
    { id: 1, title: "Si"}
  ];

  $scope.userIdMD5 = md5.createHash(localStorage.getItem("idcandidate") || '');

  /*
  //Catalogo para Areas de empleo
  $http({
    url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/getPositionArea',
    method: "GET",
  })
  .success(function(data, status, headers, config) {
    $scope.showResponse = true;
    $scope.jobPositions = angular.fromJson(data); 
    $scope.config = config;
  })
  .error(function(data, status,headers,config) {
    $scope.config = config;
  });

  //Catalogo de Paises
  $http({
    url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/getLocalCountries',
    method: "GET",
  })
  .success(function(data, status, headers, config) {
    $scope.showResponse = true;
    $scope.countries = angular.fromJson(data); 
    $scope.config = config;
  })
  .error(function(data, status,headers,config) {
    $scope.config = config;
  });

  //Catalogos de tipos de contrato
  $http({
    url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/getContractType',
    method: "GET",
  })
  .success(function(data, status, headers, config) {
    $scope.showResponse = true;
    $scope.contracts = angular.fromJson(data); 
    $scope.config = config;
  })
  .error(function(data, status,headers,config) {
    $scope.config = config;
  });

  //Catalogo de Tipos de Cargos
  $http({
    url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/getPositionGroup',
    method: "GET",
  })
  .success(function(data, status, headers, config) {
    $scope.showResponse = true;
    $scope.positions = angular.fromJson(data); 
    $scope.config = config;
  })
  .error(function(data, status,headers,config) {
    $scope.config = config;
  });

  //Catalogo de rubro de empresas
  $http({
    url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/getActivity',
    method: "GET",
  })
  .success(function(data, status, headers, config) {
    $scope.showResponse = true;
    $scope.activities = angular.fromJson(data); 
    $scope.config = config;
  })
  .error(function(data, status,headers,config) {
    $scope.config = config;
  });
  */
  $scope.updatedSuccess = false;
  $scope.updatedError = false;
  $scope.settingsData = {};
  $scope.data = "";
  $scope.config = "";
  
  $scope.updateSettings = function() {
    $scope.notificationMean = document.getElementById("notificationMean").value;
    $scope.notificationView = document.getElementById("notificationView").value;// == 0 ? false : true;
    $scope.searchJob = document.getElementById("searchJob").value;// == 0 ? false : true;

    //alert(localStorage.getItem("idcandidate") + "\n" + $scope.notificationMean + "\n" + $scope.notificationView  + "\n" + $scope.searchJob);

    $http({
      url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/updateUserInfo',
      method: "POST",
      data: $httpParamSerializerJQLike({
        "idcandidate": localStorage.getItem("idcandidate"),
        "donde": $scope.notificationMean,
        "notifyviews": $scope.notificationView,
        "busco": $scope.searchJob,
      }),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
    })
    .success(function(data, status, headers, config) {
      $scope.updatedSuccess = true;
      $scope.data = angular.fromJson(data);
      $scope.config = config;
      localStorage.setItem("searching", $scope.searchJob);
      localStorage.setItem("wherenotify", $scope.notificationMean);
      localStorage.setItem("notifyviews", $scope.notificationView); 
    })
    .error(function(data, status,headers,config) {
      $scope.updatedError = true;
      $scope.data = angular.fromJson(data);
      $scope.config = config;
    });
  };  

  $scope.getJobSelected = function(searchId){
    if(searchId == localStorage.getItem("searching")){
      return localStorage.getItem("searching");
    }
  };

  $scope.getNotificationMeanSelected = function(notificationId){
    if(notificationId == localStorage.getItem("wherenotify")){
      return localStorage.getItem("wherenotify");
    }
  };

  $scope.getNotificationViewSelected = function(notificationId){
    if(notificationId == localStorage.getItem("notifyviews")){
      return localStorage.getItem("notifyviews");
    }
  };

  $scope.goToUserPreferences = function(){
    document.addEventListener("deviceready", function () {
      $cordovaInAppBrowser.open('http://www.konectaempleo.com/loggedapp/mycv7?idkey='+$scope.userIdMD5, '_blank')
        .then(function(event) {
          // success
        })
        .catch(function(event) {
          // error
        });
    }, false);
  };  

})

.controller('NotificationsCtrl', function($scope, $localStorage, $cookies, $timeout, $http, $log, $httpParamSerializerJQLike, $stateParams, $state, $ionicLoading, $ionicNavBarDelegate, $location, $ionicHistory) {

  $scope.notificationViewResponse = false;
  $scope.notificationPostResponse = false;
  $scope.notificationKorreoResponse = false;
  $scope.oneMatch = false;

  $http({
    url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/getNotifyKorreoByUser',
    method: "POST",
    data: $httpParamSerializerJQLike({
      "idcandidate": localStorage.getItem("idcandidate")
    }),
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
  .success(function(data, status, headers, config) {
    $scope.showResponse = true;
    $scope.config = config;
    $scope.messages = angular.fromJson(data); 
  })
  .error(function(data, status,headers,config) {
    $scope.showResponse = false;
    $scope.noResults = true;
    $scope.config = config;
  })
  .finally(function(){
    $ionicLoading.hide();
    if(!Array.isArray($scope.messages)){
      $scope.oneMatch = true;
      $scope.singleMessage = $scope.messages;
      $scope.showResponse = false;
      $scope.noResults = true;
    }
  });   

  $scope.goToMessage = function(id, subject, content, sentdate, id_post)
  {
    localStorage.setItem("idmessage", id);
    localStorage.setItem("message_subject", subject);
    localStorage.setItem("message_content", content);
    localStorage.setItem("message_sentdate", sentdate);
    localStorage.setItem("message_post", id_post);
    $state.go("app.message")
  };

})

.controller('NotificationCtrl', function($scope, $localStorage, $cookies, $timeout, $http, $log, $httpParamSerializerJQLike, $stateParams, $state, $ionicLoading, $ionicNavBarDelegate, $location, $ionicHistory) {
  $scope.notification = { content: "Uno ictu cruentus ille utrimque - statuit American Mexicanus gubernationes contra CONEXUS, et amputavit auriculam eius generis copia methamphetamine ad Africum.", id: 1 }
})

.controller('MessagesCtrl', function($scope, $localStorage, $cookies, $timeout, $http, $log, $httpParamSerializerJQLike, $stateParams, $state, $ionicLoading, $ionicNavBarDelegate, $location, $ionicHistory) {
  $scope.messages = '';
  $scope.config = '';
  $scope.showResponse = false;
  $scope.noResults = false;
  $scope.oneMatch = false;
  
  $ionicLoading.show({
    content: 'Loading',
    animation: 'fade-in',
    showBackdrop: true,
    maxWidth: 400,
    showDelay: 0
  });

  $http({
    url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/getNotifyKorreoByUser',
    method: "POST",
    data: $httpParamSerializerJQLike({
      "idcandidate": localStorage.getItem("idcandidate")
    }),
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
  .success(function(data, status, headers, config) {
    $scope.showResponse = true;
    $scope.config = config;
    $scope.messages = angular.fromJson(data); 
  })
  .error(function(data, status,headers,config) {
    $scope.showResponse = false;
    $scope.noResults = true;
    $scope.config = config;
  })
  .finally(function(){
    $ionicLoading.hide();
    if(!Array.isArray($scope.messages)){
      $scope.oneMatch = true;
      $scope.singleMessage = $scope.messages;
      $scope.showResponse = false;
      $scope.noResults = false;
    }
  });   
  
  $scope.goToMessage = function(id, subject, content, sentdate, id_post)
  {
    localStorage.setItem("idmessage", id);
    localStorage.setItem("message_subject", subject);
    localStorage.setItem("message_content", content);
    localStorage.setItem("message_sentdate", sentdate);
    localStorage.setItem("message_post", id_post);
    $state.go("app.message")
  };
})

.controller('MessageCtrl', function($scope, $localStorage, $cookies, $timeout, $http, $log, $httpParamSerializerJQLike, $stateParams, $state, $ionicLoading, $ionicNavBarDelegate, $location, $ionicHistory) {
  $scope.results = '';
  $scope.config = '';
  $scope.showResponse = false;
  $scope.noResults = false;
  $scope.oneMatch = false;
  
  $scope.idmessage = localStorage.getItem("idmessage")
  $scope.message_subject = localStorage.getItem("message_subject")
  $scope.message_content = localStorage.getItem("message_content")
  $scope.message_sentdate = localStorage.getItem("message_sentdate")
  $scope.message_post = localStorage.getItem("message_post")

  $scope.showAction = true;
  if($scope.message_post == 0 || $scope.message_post == undefined){
    $scope.showAction = false;
  }

  $ionicLoading.show({
    content: 'Loading',
    animation: 'fade-in',
    showBackdrop: true,
    maxWidth: 400,
    showDelay: 0
  });



  $http({
    url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/readKorreos',
    method: "POST",
    data: $httpParamSerializerJQLike({
      "idkca": localStorage.getItem("idmessage")
    }),
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
  .success(function(data, status, headers, config) {
    $scope.showResponse = true;
  })
  .error(function(data, status,headers,config) {
    $scope.showResponse = false;
  })
  .finally(function(){
    $ionicLoading.hide();
  });     

  $scope.openJob = function(id){
    localStorage.setItem("jobId", id);
    $state.go('app.job', {jobId: id});
  };

})

.controller('DashboardCtrl', function($scope, $localStorage, $cookies, $stateParams, $state, $http, $httpParamSerializerJQLike, $ionicLoading, $ionicNavBarDelegate, $location, $ionicHistory, $timeout, md5, $cordovaInAppBrowser) {
  

    $http({
      url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/getCountNewNotifyByUser',
      method: "POST",
      data: $httpParamSerializerJQLike({
        "idcandidate": localStorage.getItem("idcandidate")
      }),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
    })
    .success(function(data, status, headers, config) {
      $scope.showResponse = true;
      $scope.notificationsNumber = angular.fromJson(data); 
      $scope.config = config;
    })
    .error(function(data, status,headers,config) {
      $scope.notificationsNumber = 0;
      $scope.showResponse = false;
      $scope.config = config;
    }); 


  $scope.showUserOptions = false;
  $scope.showExtraPreferenceButton = false;
  $ionicHistory.nextViewOptions({
    disableBack: true
  });
  
  $scope.hasPicture = localStorage.getItem("picture") != null ? true : false;
  $scope.userPic = localStorage.getItem("picture");
  $scope.apply = 0;
  $scope.appliedJob = false;
  if($scope.hasPicture == true && ($scope.userPic == null || $scope.userPic == 'null')){
    $scope.hasPicture = false;
  }
  $scope.deviceToken = localStorage.getItem("device_token");
  $scope.apply = localStorage.getItem("apply");
            
  if($scope.apply == 1){
    localStorage.setItem("apply", 0);
    $scope.appliedJob = true;
    $timeout(function() {
      $scope.appliedJob = false;
    }, 6000);
  }
  //alert($scope.apply);
  if(localStorage.getItem("idcandidate")!= null){
    $scope.showUserOptions = true;
  }


  $scope.welcome = $stateParams.welcome;
  $scope.data = '';
  $scope.config = '';
  $scope.showResponse = false;
  $scope.jobsSearchMsg = "No se encontraron plazas";
  $scope.settingsData = {}
  $scope.name = localStorage.getItem("name");
  $scope.lastname = localStorage.getItem("lastname");
  $scope.idcanditate = localStorage.getItem("idcandidate");
  // Si la ruta actual es jobs carga los trabajos 
  // Si el usuario esta loggeado muestro los trabajos segun sus preferencias
  $ionicLoading.show({
    content: 'Loading',
    animation: 'fade-in',
    showBackdrop: true,
    maxWidth: 400,
    showDelay: 0
  });

  if(localStorage.getItem("idcandidate")!= null || localStorage.getItem("idcandidate")!=undefined){
    $scope.fullname = $scope.name + " " + $scope.lastname;
    // TODO cargar empleos segun preferencias
    $http({
      url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/getPostPreferences',
      method: "POST",
      data: $httpParamSerializerJQLike({
        "idcandidate": localStorage.getItem("idcandidate")
      }),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
    })
    .success(function(data, status, headers, config) {
      $scope.jobsSearchMsg = "Empleos de acuerdo a tus preferencias";
      $scope.showResponse = true;
      $scope.data = angular.fromJson(data); 
      //alert($scope.data.id_post);
      $scope.config = config;
    })
    .error(function(data, status,headers,config) {
      $scope.showResponse = false;
      $scope.data = angular.fromJson(data);
      $scope.config = config;
      $scope.jobsSearchMsg = "No se encontraron plazas"
    })
    .finally(function(){
      $ionicLoading.hide();
      $scope.showUserOptions = true;
      if($scope.data == undefined){
        $scope.jobsSearchMsg = "No hemos encontrado empleos de acuerdo a tus preferencias"
        $scope.showExtraPreferenceButton = true;
      }
    }); 
    /* SI NO ENCUENTRO PLAZAS
    if($scope.data.id_post == undefined || $scope.data.id_post == null){
      $http({
        url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/getAllPosts',
        method: "GET",
      })
      .success(function(data, status, headers, config) {
        $scope.jobsSearchMsg = "Listado de empleos";
        $scope.showResponse = true;
        $scope.data = angular.fromJson(data); 
        $scope.config = config;
      })
      .error(function(data, status,headers,config) {
        $scope.showResponse = false;
        $scope.jobsSearchMsg = "No se encontraron plazas";
      })
      .finally(function(){
        $ionicLoading.hide();
      });
    } */
  }
  else{
    $scope.fullname = "Invitado(a)";

    $http({
      url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/getAllPosts',
      method: "GET",
    })
    .success(function(data, status, headers, config) {
      $scope.showResponse = true;
      $scope.data = angular.fromJson(data); 
      $scope.config = config;
      $scope.jobsSearchMsg = "Resultado de la busqueda"
    })
    .error(function(data, status,headers,config) {
      $scope.showResponse = false;
      $scope.data = angular.fromJson(data);
      $scope.config = config;
    })
    .finally(function(){
      $ionicLoading.hide();
    });
  }

  $scope.getPosts = function(){
    $http({
      url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/getAllPosts',
      method: "GET",
    })
    .success(function(data, status, headers, config) {
      $scope.showResponse = true;
      $scope.data = angular.fromJson(data); 
      $scope.config = config;
    })
    .error(function(data, status,headers,config) {
      $scope.showResponse = false;
      $scope.NoJobsFound = true;
    });
  };
  
  $scope.userIdMD5 = md5.createHash(localStorage.getItem("idcandidate") || '');

  $scope.goToUserInfo = function(){
    document.addEventListener("deviceready", function () {
      $cordovaInAppBrowser.open('http://www.konectaempleo.com/loggedapp/mycv2?idkey='+$scope.userIdMD5, '_blank')
        .then(function(event) {
          // success
        })
        .catch(function(event) {
          // error
        });
    }, false);
  };
  $scope.goToUserJobs = function(){
    document.addEventListener("deviceready", function () {
      $cordovaInAppBrowser.open('http://www.konectaempleo.com/loggedapp/mycv3?idkey='+$scope.userIdMD5, '_blank')
        .then(function(event) {
          // success
        })
        .catch(function(event) {
          // error
        });
    }, false);
  };
  $scope.goToUserStudies = function(){
    document.addEventListener("deviceready", function () {
      $cordovaInAppBrowser.open('http://www.konectaempleo.com/loggedapp/mycv4?idkey='+$scope.userIdMD5, '_blank')
        .then(function(event) {
          // success
        })
        .catch(function(event) {
          // error
        });
    }, false);
  };
  $scope.goToUserSkills = function(){
    document.addEventListener("deviceready", function () {
      $cordovaInAppBrowser.open('http://www.konectaempleo.com/loggedapp/mycv6?idkey='+$scope.userIdMD5, '_blank')
        .then(function(event) {
          // success
        })
        .catch(function(event) {
          // error
        });
    }, false);
  };
  $scope.goToUserPreferences = function(){
    document.addEventListener("deviceready", function () {
      $cordovaInAppBrowser.open('http://www.konectaempleo.com/loggedapp/mycv7?idkey='+$scope.userIdMD5, '_blank')
        .then(function(event) {
          // success
        })
        .catch(function(event) {
          // error
        });
    }, false);
  };



  $scope.openJob = function(id){
    localStorage.setItem("jobId", id);
    $state.go('app.job', {jobId: id});
  };
})

.controller('JobsCtrl', function($scope, $localStorage, $cookies, $stateParams, $state, $http, $httpParamSerializerJQLike, $location, $ionicLoading, $ionicNavBarDelegate, $location, $ionicHistory) {


    $http({
      url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/getCountNewNotifyByUser',
      method: "POST",
      data: $httpParamSerializerJQLike({
        "idcandidate": localStorage.getItem("idcandidate")
      }),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
    })
    .success(function(data, status, headers, config) {
      $scope.showResponse = true;
      $scope.notificationsNumber = angular.fromJson(data); 
      $scope.config = config;
    })
    .error(function(data, status,headers,config) {
      $scope.notificationsNumber = 0;
      $scope.showResponse = false;
      $scope.config = config;
    }); 

  $scope.data = '';
  $scope.config = '';
  $scope.showResponse = false;
  $scope.NoJobsFound = false;

  $scope.deviceToken = localStorage.getItem("device_token");
  $ionicLoading.show({
    content: 'Loading',
    animation: 'fade-in',
    showBackdrop: true,
    maxWidth: 400,
    showDelay: 0
  });
  
  $http({
    url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/getAllPosts',
    method: "GET",
  })
  .success(function(data, status, headers, config) {
    $scope.showResponse = true;
    $scope.jobs = angular.fromJson(data); 
    $scope.config = config;
  })
  .error(function(data, status,headers,config) {
    $scope.showResponse = false;
    $scope.NoJobsFound = true;
    $scope.config = config;
  })
  .finally(function(){
    $ionicLoading.hide();
  });

  $scope.openJob = function(id){
    localStorage.setItem("jobId", id);
    $state.go('app.job', {jobId: id});
  };
})

.controller('SearchCtrl', function($scope, $localStorage, $cookies, $stateParams, $state, $http, $httpParamSerializerJQLike, $location, $ionicLoading, $ionicNavBarDelegate, $location, $ionicHistory){
  //$scope.$on('$ionicView.beforeEnter', function (event, viewData) {
  //  viewData.enableBack = true;
  //}); 

  $http({
    url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/getCountNewNotifyByUser',
    method: "POST",
    data: $httpParamSerializerJQLike({
      "idcandidate": localStorage.getItem("idcandidate")
    }),
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
  })
  .success(function(data, status, headers, config) {
    $scope.showResponse = true;
    $scope.notificationsNumber = angular.fromJson(data); 
    $scope.config = config;
  })
  .error(function(data, status,headers,config) {
    $scope.notificationsNumber = 0;
    $scope.showResponse = false;
    $scope.config = config;
  }); 

  $scope.settingsData = {};
  $scope.showResponse = false;
  $scope.noResults = true;



  $scope.openJob = function(id){
    localStorage.setItem("jobId", id);
    $state.go('app.job', {jobId: id});
  };

})

.controller('SearchCountryCtrl', function($scope, $localStorage, $cookies, $stateParams, $state, $http, $httpParamSerializerJQLike, $location, $ionicLoading, $ionicNavBarDelegate, $location, $ionicHistory){
  $scope.settingsData = {};
  $scope.showResponse = false;
  $scope.noResults = true;

  $ionicLoading.show({
    content: 'Loading',
    animation: 'fade-in',
    showBackdrop: true,
    maxWidth: 400,
    showDelay: 0
  });

  //Catalogo de Paises
  $http({
    url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/getLocalCountries',
    method: "GET",
  })
  .success(function(data, status, headers, config) {
    $scope.showResponse = true;
    $scope.countries = angular.fromJson(data); 
    $scope.config = config;
  })
  .error(function(data, status,headers,config) {
    $scope.config = config;
  })
  .finally(function(){
    $ionicLoading.hide();
  });

  //getPostByCountry, getPostByPositionArea, getPostByActivity, getPostByContratType, getPostByPositionGroup
  $scope.searchCountry = function(){
    $scope.jobs = {}
    $ionicLoading.show({
      content: 'Loading',
      animation: 'fade-in',
      showBackdrop: true,
      maxWidth: 400,
      showDelay: 0
    });
    var countryw = document.getElementById("country");
    $scope.country = countryw.options[countryw.selectedIndex].value;
    $http({
      url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/getPostByCountry',
      method: "POST",
      data: $httpParamSerializerJQLike({
        "idcountry": $scope.country
      }),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
    })
    .success(function(data, status, headers, config) {
      $scope.jobs = angular.fromJson(data); 
      $scope.config = config;
      $scope.showResponse =true;
    })
    .error(function(data, status,headers,config) {
      $scope.config = config;
      $scope.noResults =true;
    })
    .finally(function(){
      $ionicLoading.hide();
    });
  };

  $scope.openJob = function(id){
    localStorage.setItem("jobId", id);
    $state.go('app.job', {jobId: id});
  };

})

.controller('SearchPACtrl', function($scope, $localStorage, $cookies, $stateParams, $state, $http, $httpParamSerializerJQLike, $location, $ionicLoading, $ionicNavBarDelegate, $location, $ionicHistory){
  $scope.settingsData = {};
  $scope.showResponse = false;
  $scope.noResults = true;

  $ionicLoading.show({
    content: 'Loading',
    animation: 'fade-in',
    showBackdrop: true,
    maxWidth: 400,
    showDelay: 0
  });

  //Catalogo por Area
  $http({
    url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/getPositionArea',
    method: "GET",
    crossDomain: true
  })
  .success(function(data, status, headers, config) {
    $scope.jobPositions = angular.fromJson(data); 
  })
  .error(function(data, status,headers,config) {
    console.log("no se pudo cargar la info");
  })
  .finally(function(){
    $ionicLoading.hide();
  });

  $scope.searchPositionArea = function(){
    $scope.jobs = {}

    $ionicLoading.show({
      content: 'Loading',
      animation: 'fade-in',
      showBackdrop: true,
      maxWidth: 400,
      showDelay: 0
    });
    var positionAreaw = document.getElementById("positionArea");
    $scope.positionArea = positionAreaw.options[positionAreaw.selectedIndex].value;
    //alert($scope.positionArea);
    $http({
      url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/getPostByPositionArea',
      method: "POST",
      data: $httpParamSerializerJQLike({
        "idpa": $scope.positionArea
      }),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
    })
    .success(function(data, status, headers, config) {
      $scope.jobs = angular.fromJson(data); 
      $scope.config = config;
      $scope.showResponse =true;
    })
    .error(function(data, status,headers,config) {
      $scope.config = config;
      $scope.noResults =true;
    })
    .finally(function(){
      $ionicLoading.hide();
    });
  };

  $scope.openJob = function(id){
    localStorage.setItem("jobId", id);
    $state.go('app.job', {jobId: id});
  };

})

.controller('SearchPGCtrl', function($scope, $localStorage, $cookies, $stateParams, $state, $http, $httpParamSerializerJQLike, $location, $ionicLoading, $ionicNavBarDelegate, $location, $ionicHistory){
  $scope.settingsData = {};
  $scope.showResponse = false;
  $scope.noResults = true;

  $ionicLoading.show({
    content: 'Loading',
    animation: 'fade-in',
    showBackdrop: true,
    maxWidth: 400,
    showDelay: 0
  });

  //Catalogo de Tipos de Cargos
  $http({
    url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/getPositionGroup',
    method: "GET",
  })
  .success(function(data, status, headers, config) {
    $scope.showResponse = true;
    $scope.positions = angular.fromJson(data); 
    $scope.config = config;
  })
  .error(function(data, status,headers,config) {
    $scope.config = config;
  })
  .finally(function(){
    $ionicLoading.hide();
  });

  $scope.searchPositionGroup = function(){
    $scope.jobs = {}

    $ionicLoading.show({
      content: 'Loading',
      animation: 'fade-in',
      showBackdrop: true,
      maxWidth: 400,
      showDelay: 0
    });
    var positionGroupw = document.getElementById("positionGroup");
    $scope.positionGroup = positionGroupw.options[positionGroupw.selectedIndex].value;
    //alert($scope.positionArea);
    $http({
      url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/getPostByPositionGroup',
      method: "POST",
      data: $httpParamSerializerJQLike({
        "idpg": $scope.positionGroup
      }),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
    })
    .success(function(data, status, headers, config) {
      $scope.jobs = angular.fromJson(data); 
      $scope.config = config;
      $scope.showResponse =true;
    })
    .error(function(data, status,headers,config) {
      $scope.config = config;
      $scope.noResults =true;
    })
    .finally(function(){
      $ionicLoading.hide();
    });
  };


  $scope.openJob = function(id){
    localStorage.setItem("jobId", id);
    $state.go('app.job', {jobId: id});
  };

})


.controller('SearchContractCtrl', function($scope, $localStorage, $cookies, $stateParams, $state, $http, $httpParamSerializerJQLike, $location, $ionicLoading, $ionicNavBarDelegate, $location, $ionicHistory){
  $scope.settingsData = {};
  $scope.showResponse = false;
  $scope.noResults = true;

  $ionicLoading.show({
    content: 'Loading',
    animation: 'fade-in',
    showBackdrop: true,
    maxWidth: 400,
    showDelay: 0
  });

   //Catalogos de tipos de contrato
  $http({
    url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/getContractType',
    method: "GET",
  })
  .success(function(data, status, headers, config) {
    $scope.showResponse = true;
    $scope.contracts = angular.fromJson(data); 
    $scope.config = config;
  })
  .error(function(data, status,headers,config) {
    $scope.config = config;
  })
  .finally(function(){
    $ionicLoading.hide();
  });
  
  $scope.searchContract = function(){
    $scope.jobs = {}

    $ionicLoading.show({
      content: 'Loading',
      animation: 'fade-in',
      showBackdrop: true,
      maxWidth: 400,
      showDelay: 0
    });
    var contractw = document.getElementById("contract");
    $scope.contract = contractw.options[contractw.selectedIndex].value;
    //alert($scope.positionArea);
    $http({
      url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/getPostByContractType',
      method: "POST",
      data: $httpParamSerializerJQLike({
        "idct": $scope.contract
      }),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
    })
    .success(function(data, status, headers, config) {
      $scope.jobs = angular.fromJson(data); 
      $scope.config = config;
      $scope.showResponse =true;
    })
    .error(function(data, status,headers,config) {
      $scope.config = config;
      $scope.noResults =true;
    })
    .finally(function(){
      $ionicLoading.hide();
    });
  };


  $scope.openJob = function(id){
    localStorage.setItem("jobId", id);
    $state.go('app.job', {jobId: id});
  };

})


.controller('MyJobsCtrl', function($scope, $localStorage, $cookies, $stateParams, $state, $http, $httpParamSerializerJQLike, $ionicLoading, $ionicNavBarDelegate, $location, $ionicHistory) {
  $scope.data = '';
  $scope.config = '';
  $scope.firstObject = {};
  $scope.showResponse = false;
  $noResults = false;

  $ionicLoading.show({
    content: 'Loading',
    animation: 'fade-in',
    showBackdrop: true,
    maxWidth: 400,
    showDelay: 0
  });

  $http({
    url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/getCountNewNotifyByUser',
    method: "POST",
    data: $httpParamSerializerJQLike({
      "idcandidate": localStorage.getItem("idcandidate")
    }),
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
  })
  .success(function(data, status, headers, config) {
    $scope.showResponse = true;
    $scope.notificationsNumber = angular.fromJson(data); 
    $scope.config = config;
  })
  .error(function(data, status,headers,config) {
    $scope.notificationsNumber = 0;
    $scope.showResponse = false;
    $scope.config = config;
  }); 


  $http({
    url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/getAppliedPost',
    method: "POST",
    data: $httpParamSerializerJQLike({
      "idcandidate": localStorage.getItem("idcandidate")
    }),
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
  })
  .success(function(data, status, headers, config) {
    $scope.showResponse = true;
    $scope.noResults = false;
    $scope.firstObject = data[0];
    $scope.data = angular.fromJson(data); 
    $scope.config = config;
  })
  .error(function(data, status,headers,config) {
    $scope.showResponse = false;
    $scope.noResults = true;
    $scope.data = angular.fromJson(data);
    $scope.config = config;
  })
  .finally(function(){
    if(!Array.isArray($scope.data)){
      $scope.noResults = true;
    }
    $ionicLoading.hide();
  });

  $scope.openJob = function(id){
    localStorage.setItem("jobId", id);
    $state.go('app.job', {jobId: id});
  };
  
  $scope.openJobProcess = function(id){
    localStorage.setItem("jobId", id);
    $state.go('app.job_process', {jobId: id});
  };

})

.controller('JobCtrl', function($scope, $localStorage, $cookies, $timeout, $http, $httpParamSerializerJQLike, $log, $stateParams, $state, $ionicLoading, $ionicNavBarDelegate, $location, $ionicHistory, $ionicHistory) {

  $scope.status = '';
  $scope.data = '';
  $scope.headers = '';
  $scope.config = '';
  $scope.response='';
  $scope.showResponse = false;
  $scope.errorOnApply = false;
  $scope.buttonEnable = true;
  $scope.showJobProcess = false;
  $scope.disableCompany = false;

  $scope.myGoBack = function() {
    $ionicHistory.goBack();
  };

  $ionicLoading.show({
    content: 'Loading',
    animation: 'fade-in',
    showBackdrop: true,
    maxWidth: 400,
    showDelay: 0
  });

  $http({
    url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/getPostById',
    method: "POST",
    data: $httpParamSerializerJQLike({
      "id": localStorage.getItem("jobId")
      //"user":{
      //  "email":"wahxxx@gmail.com",
      //  "password":"123456"
      //}
    }),
    /*data: $httpParamSerializerJQLike({
      'id': localStorage.getItem("jobId")
    }),*/
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
  })
  .success(function(data, status, headers, config) {
    $scope.showResponse = true;
    $scope.data = angular.fromJson(data); 
    localStorage.setItem("idcompany", $scope.data.id_company);
    $scope.config = config;
  })
  .error(function(data, status,headers,config) {
    $scope.showResponse = false;
    $scope.data = angular.fromJson(data);
    $scope.config = config;
  })
  .finally(function(){
    $ionicLoading.hide();
  });

  $http({
    url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/getCompanyInfo',
    method: "POST",
    data: $httpParamSerializerJQLike({
      "idcompany": localStorage.getItem("idcompany")
    }),
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
  })
  .success(function(data, status, headers, config) {
    $scope.showResponse = true;
    $scope.company = angular.fromJson(data); 
    $scope.config = config;
  })
  .error(function(data, status,headers,config) {
    $scope.showResponse = false;
    $scope.config = config;
  })
  .finally(function(){
    if($scope.company.hideinfo == 1 || $scope.company.hideinfo == "1" || localStorage.getItem("idcompany") == 0){
      $scope.disableCompany = true;
    }
  });   


  if(localStorage.getItem("idcandidate")!=undefined || localStorage.getItem("idcandidate")!=null){
    $http({
      url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/getApply',
      method: "POST",
      data: $httpParamSerializerJQLike({
        "idk": localStorage.getItem("idcandidate"),
        "idpost": localStorage.getItem("jobId")
      }),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
    })
    .success(function(data, status, headers, config) {
      $scope.response = data; 
    })
    .error(function(data, status,headers,config) {
      $scope.response = data;
      $scope.config = config;
    })
    .finally(function(){
      if($scope.response != "No has aplicado"){
        $scope.buttonEnable = false;
        $scope.showJobProcess = true;
      }
    })
  }

  $scope.applyJob = {};
  $scope.applyToJob = function() {
    $ionicLoading.show({
      content: 'Loading',
      animation: 'fade-in',
      showBackdrop: true,
      maxWidth: 400,
      showDelay: 0
    });
    if(localStorage.getItem("idcandidate")==undefined || localStorage.getItem("idcandidate")==null){
      $ionicLoading.hide();
      $state.go('register_index', {reload: true});
    }
    else{
      $http({
        url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/insertPostApply',
        method: "POST",
        data: $httpParamSerializerJQLike({
          "idpost": localStorage.getItem("jobId"),
          "idcandidate": localStorage.getItem("idcandidate"),
        }),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
      })
      .success(function(data, status, headers, config) {
        $scope.showResponse = true;
        $scope.appliedJob = data; 
        $scope.config = config;
      })
      .error(function(data, status,headers,config) {
        $scope.showResponse = false;
        $scope.config = config;
      })
      .finally(function(){
        $ionicLoading.hide();
        //alert("\n" + $scope.appliedJob);
        if($scope.appliedJob == "Postulacion Exitosa"){
          //alert("HERE APPLIED");
          localStorage.setItem("apply", 1);
          $state.go("app.dashboard");
        }
        else{
          $scope.showResponse = false;
        }
      });  
    }
  };  

  $scope.openCompany = function(id){
    localStorage.setItem("idcompany", id);
    $state.go('app.company', {jobId: localStorage.getItem("jobId"), companyID: id});
  };

  $scope.openJobProcess = function(id){
    $state.go('app.job_process', {jobId: localStorage.getItem("jobId")});
  };

})

.controller('AppliedJobCtrl', function($scope, $localStorage, $cookies, $timeout, $http, $httpParamSerializerJQLike, $log, $stateParams, $state, $ionicLoading, $ionicNavBarDelegate, $location, $ionicHistory, $ionicHistory) {

  $scope.status = '';
  $scope.data = '';
  $scope.headers = '';
  $scope.config = '';
  $scope.response='';
  $scope.showResponse = false;
  $scope.buttonEnable = true;
  $scope.oneMatch = false;

  $scope.myGoBack = function() {
    $ionicHistory.goBack();
  };

  $ionicLoading.show({
    content: 'Loading',
    animation: 'fade-in',
    showBackdrop: true,
    maxWidth: 400,
    showDelay: 0
  });

  $http({
    url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/getCandidateStageByPost',
    method: "POST",
    data: $httpParamSerializerJQLike({
      "idpost": localStorage.getItem("jobId"),
      "idcandidate": localStorage.getItem("idcandidate")
      //"user":{
      //  "email":"wahxxx@gmail.com",
      //  "password":"123456"
      //}
    }),
    /*data: $httpParamSerializerJQLike({
      'id': localStorage.getItem("jobId")
    }),*/
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
  })
  .success(function(data, status, headers, config) {
    $scope.showResponse = true;
    $scope.data = angular.fromJson(data); 
    $scope.config = config;
  })
  .error(function(data, status,headers,config) {
    $scope.showResponse = false;
    $scope.data = angular.fromJson(data);
    $scope.config = config;
  })
  .finally(function(){
    if(!Array.isArray($scope.data)){
      $scope.singleMessage = $scope.data;
      $scope.oneMatch = true;
    }
    $ionicLoading.hide();
  });

})

.controller('CompanyCtrl', function($scope, $localStorage, $cookies, $timeout, $http, $httpParamSerializerJQLike, $log, $stateParams, $state, $ionicLoading, $ionicNavBarDelegate, $location, $ionicHistory) {
  $scope.status = '';
  $scope.company = '';
  $scope.headers = '';
  $scope.config = '';
  $scope.showResponse = false;

  $ionicLoading.show({
    content: 'Loading',
    animation: 'fade-in',
    showBackdrop: true,
    maxWidth: 400,
    showDelay: 0
  });

  $http({
    url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/getCompanyInfo',
    method: "POST",
    data: $httpParamSerializerJQLike({
      "idcompany": localStorage.getItem("idcompany")
    }),
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
  })
  .success(function(data, status, headers, config) {
    $scope.showResponse = true;
    $scope.company = angular.fromJson(data); 
    $scope.config = config;
  })
  .error(function(data, status,headers,config) {
    $scope.showResponse = false;
    $scope.config = config;
  })
  .finally(function(){
    $ionicLoading.hide();
  });   

  $http({
    url: 'http://www.konectaempleo.com/api/webservice_v1/kecontroller/getPostByCompany',
    method: "POST",
    data: $httpParamSerializerJQLike({
      "idcompany": localStorage.getItem("idcompany")
    }),
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
  })
  .success(function(data, status, headers, config) {
    $scope.showResponse = true;
    $scope.jobs = angular.fromJson(data); 
    $scope.config = config;
  })
  .error(function(data, status,headers,config) {
    $scope.showResponse = false;
    $scope.config = config;
  })
  .finally(function(){
    
  });   


  $scope.openJob = function(id){
    localStorage.setItem("jobId", id);
    $state.go('app.job', {jobId: id});
  };


});